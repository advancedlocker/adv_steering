/* A simple server in the internet domain using TCP.
myServer.c
D. Thiebaut
Adapted from http://www.cs.rpi.edu/~moorthy/Courses/os98/Pgms/socket.html
The port number used in 51717.
This code is compiled and run on the Raspberry as follows:
   
    g++ -o myServer myServer.c 
    ./myServer

The server waits for a connection request from a client.
The server assumes the client will send positive integers, which it sends back multiplied by 2.
If the server receives -1 it closes the socket with the client.
If the server receives -2, it exits.
*/

#include <stdio.h>
#include <sys/types.h> 
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <bcm2835.h>
#include "UART_Handler.h"
#include "Server_Handler.h"
#include "Lock_Cont_Handler.h"

#define BUFFER_LEN		256

int main(int argc, char *argv[]) 
{  
	bcm2835_init();
	s_comm_port_t UART_Comm;
	
	Init_Server_Handler(2031);		
	Init_Lock_Cont_Handler(&UART_Comm);
  
	while(1) 
	{		 
		SERVER_TASK();		
		Lock_Cont_Task();
	}

	bcm2835_close();
	
	return 0; 
}