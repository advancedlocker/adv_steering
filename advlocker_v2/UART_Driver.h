/* 
 * File:   serial_port.h
 * Author: Patrick
 *
 * Created on March 31, 2016, 12:15 PM
 */

#ifndef SERIAL_PORT_H
#define	SERIAL_PORT_H

#ifdef	__cplusplus
extern "C"
{
#endif	

int open_serial_port(char *device, int buad, int timeout);

int read_serial_port(int fd, unsigned char *data, int len);

int write_serial_port(int fd, unsigned char *data, int len);

int reset_serial_port(int fd);

int close_serial_port(int fd);

#ifdef	__cplusplus
}
#endif

#endif	/* NEW_SERIAL_H */

