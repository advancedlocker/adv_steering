/* 
 * File:   lib_uptime.h
 * Author: Patrick
 *
 * Created on June 27, 2016, 7:14 PM
 */

#ifndef LIB_UPTIME_H
#define	LIB_UPTIME_H

#ifdef	__cplusplus
extern "C"
{
#endif
	
#include <stdint.h>
/* This module implements an interface to allow retrieval of the uptime
 * of the box in milliseconds.  This is usually used for timing applications.
 * Note that due to the limited range of the uptime_t type, it can
 * potentially overflow.
 *
 * A uptime_timer_t type is implemented which provides a very simple
 * timer mechanism using the uptime.  As opposed to an sstimer_t, the
 * only resources required to use one of these timers is the memory
 * for the variable - it does not consume any runtime.  The uptime_timer_t
 * does not need to be allocated/deallocated making it ideal for
 * use on the stack.  It is typically used in applications which 
 * load the timer and then poll for its expiry.  An uptime_timer_t
 * is typically more expensive to check for expiry than an sstimer_t.
 */


/* The version of the firmware module. Upper 24 bits are major and last byte is minor versions.
 */
#define LIB_UPTIME_MODULE_VERSION			0x0010


/* Maximum value that can be held by an uptime_t.
 */
#define UPTIME_T_MAX        UINT32_MAX

/* Type to hold uptime in milliseconds.  If this type is changed, ensure
 * that get_uptime_ms() is also modified to obtain value atomically.
 */
	

typedef uint32_t uptime_t;


/* Type used to do timing based on the uptime value.  These timers are very quick
 * to load and need no processing power to tick.  Checking the expiry of a timer
 * takes more processing power than other timer systems.
 */
typedef struct {
    uptime_t uptime;
    uint8_t wrap:1;
    uint8_t is_started:1;
} uptime_timer_t;

/* Number of milliseconds that unit has been operational.  Should
 * ONLY be accessed through get_uptime_ms() function.
 */

//extern uptime_t uptime_ms_;

uint16_t LIB_UPTIME_get_module_version(void);

/*!
 * \brief Obtain current uptime atomically.  Use this rather than accessing
 * uptime directly to avoid irq races.  This may be optimised eventually
 * for each architecture.
 * \return Current uptime in milliseconds.
 */
uptime_t get_uptime_ms(void);

/*!
 * \brief Gets current uptime without any locking.
 * \return Current uptime in milliseconds.
 */
uptime_t get_uptime_ms_(void);


/*!
 * \brief Loads an uptimer_timer_t timer with the specified delay.
 * \param timer Timer to load.
 * \param delay_ms Timeout milliseconds.
 */
void uptime_timer_start(uptime_timer_t * timer, uint32_t delay_ms);

/*!
 * \brief Checks a timer for expiry.
 * \param timer Timer to check for expiry.
 * \return non-zero Timer expired, zero Timer not expired.
 */

unsigned long uptime_timer_is_expired(uptime_timer_t * timer);

/*!
 * \brief Stops a timer by marking its started flag as 0.
 * \param timer Timer to stop.
 */
void uptime_timer_stop(uptime_timer_t * timer);

/*!
 * \brief Returns non-zero if the specified time has been started.
 * This will return non-zero even if the timer is expired.
 */
int uptime_timer_is_started(uptime_timer_t * timer);

/*!
 * \brief Returns the number of milliseconds remaning on the timer
 * Returns the number of milliseconds remaning on the timer
 */
int32_t uptime_timer_remaining(uptime_timer_t * timer);



#ifdef	__cplusplus
}
#endif

#endif	/* LIB_UPTIME_H */

