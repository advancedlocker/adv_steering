/*!
 * \file lib_uptime.c
 *
 * Architecture independent uptime routines.
 *
 * Copyright (c) 2014 Diverse Electronic Solutions Pty Ltd
 * Developed by Austin Phillips, aussie_phillips@yahoo.com.au
 *
 * All rights reserved. No part of this file may be reproduced, published,
 * distributed, displayed, performed, copied or stored for public or private
 * use in any information retrieval system, or transmitted in any form by any
 * mechanical, photographic or electronic process, including electronically or
 * digitally on the Internet or World Wide Web, or over any network, or local
 * area network, without written permission of Diverse Electronic Solutions Pty Ltd.
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <math.h>
#include <time.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
//#include "config.h"
#include "lib_uptime.h"

#include "stdlib.h"

/* Global uptime variable.  On most architectures this is probably
 * updated by timer interrupt service routine.
 */

uint32_t uptime_ms_ = 0;

uint32_t uptime_ns_ = 0;

uint32_t temp_uptime_ns_ = 0;	


uint16_t LIB_UPTIME_get_module_version(void)
{
	return (LIB_UPTIME_MODULE_VERSION);
}

uptime_t get_uptime_ms(void)
{		
	uint32_t read_uptime_ns_ = 0;	
	
	struct timespec gettime_now;
	
	clock_gettime(CLOCK_REALTIME, &gettime_now);
	
	read_uptime_ns_ = gettime_now.tv_nsec;//Get nS value	
	
	uptime_ns_ += (read_uptime_ns_ - temp_uptime_ns_);	
	
	temp_uptime_ns_ = read_uptime_ns_;
	
	if(uptime_ns_ > 1000000){
		
		uptime_ms_++;	
		
		uptime_ns_ = 0;
	}	
	//printf("get_uptime_ms	%ld\n", uptime_ms_);	
	
    return uptime_ms_;
}

void uptime_timer_start(uptime_timer_t * timer, uint32_t delay_ms)
{
    uptime_t ctime = get_uptime_ms();

    timer->uptime = ctime + delay_ms;

    if (timer->uptime < ctime)
        timer->wrap = 1;
    else
        timer->wrap = 0;

    timer->is_started = 1;
}

unsigned long uptime_timer_is_expired(uptime_timer_t * timer){
	
    uptime_t ctime = get_uptime_ms();

    if (timer->wrap) {
        if (ctime > timer->uptime)
            return 0;

        /* uptime_ms_ has wrapped.
        */
        timer->wrap = 0;
        return 0;
    }
	
    return ctime >= timer->uptime;
}

void uptime_timer_stop(uptime_timer_t * timer)
{
    timer->is_started = 0;
}

int uptime_timer_is_started(uptime_timer_t * timer)
{
    return timer->is_started;
}

int32_t uptime_timer_remaining(uptime_timer_t * timer)
{
    int32_t remain = 0;

    if(timer->is_started)
        remain = timer->uptime - get_uptime_ms();

    return remain;
}

