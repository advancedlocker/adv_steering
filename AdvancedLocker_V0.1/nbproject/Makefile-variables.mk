#
# Generated - do not edit!
#
# NOCDDL
#
CND_BASEDIR=`pwd`
CND_BUILDDIR=build
CND_DISTDIR=dist
# Debug configuration
CND_PLATFORM_Debug=GNU-Linux-x86
CND_ARTIFACT_DIR_Debug=dist/Debug/GNU-Linux-x86
CND_ARTIFACT_NAME_Debug=advancedlocker_v0.1
CND_ARTIFACT_PATH_Debug=dist/Debug/GNU-Linux-x86/advancedlocker_v0.1
CND_PACKAGE_DIR_Debug=dist/Debug/GNU-Linux-x86/package
CND_PACKAGE_NAME_Debug=advancedlockerv0.1.tar
CND_PACKAGE_PATH_Debug=dist/Debug/GNU-Linux-x86/package/advancedlockerv0.1.tar
# Release configuration
CND_PLATFORM_Release=GNU-Linux-x86
CND_ARTIFACT_DIR_Release=dist/Release/GNU-Linux-x86
CND_ARTIFACT_NAME_Release=advancedlocker_v0.1
CND_ARTIFACT_PATH_Release=dist/Release/GNU-Linux-x86/advancedlocker_v0.1
CND_PACKAGE_DIR_Release=dist/Release/GNU-Linux-x86/package
CND_PACKAGE_NAME_Release=advancedlockerv0.1.tar
CND_PACKAGE_PATH_Release=dist/Release/GNU-Linux-x86/package/advancedlockerv0.1.tar
#
# include compiler specific variables
#
# dmake command
ROOT:sh = test -f nbproject/private/Makefile-variables.mk || \
	(mkdir -p nbproject/private && touch nbproject/private/Makefile-variables.mk)
#
# gmake command
.PHONY: $(shell test -f nbproject/private/Makefile-variables.mk || (mkdir -p nbproject/private && touch nbproject/private/Makefile-variables.mk))
#
include nbproject/private/Makefile-variables.mk
