#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/time.h>
#include <sys/types.h>
#include <termio.h>
#include <time.h>
#include <linux/ioctl.h>
#include <sys/select.h>
#include <string.h>
#include <bcm2835.h>
#include <math.h>

#include "UART_Handler.h"
#include "UART_Driver.h"
#include "lib_uptime.h"

#define DEBUG_MODE	0

#define BUFFER		255

uptime_timer_t	inter_byte_timer;
uptime_t		inter_byte_timout = 1;		

//-----------------------delay ms without sleep---------------------------------

void DelayMillisecondsNoSleep(int delay_ms){
	
	long int start_time;
	
	long int time_difference;
	
	struct timespec gettime_now;

	clock_gettime(CLOCK_REALTIME, &gettime_now);
	
	start_time = gettime_now.tv_nsec;	//Get nS value
	
	while(1){
		clock_gettime(CLOCK_REALTIME, &gettime_now);
		time_difference = gettime_now.tv_nsec - start_time;
		if (time_difference < 0)
			time_difference += 1000000000;	//(Rolls over every 1 second)
		if (time_difference > (delay_ms * 1000000))	//Delay for # nS
			break;
	}
	return;
}

int setup_comm_port(s_comm_port_t *port)
{					
	memset(port, 0, sizeof(s_comm_port_t));//clr serial settings
	sprintf(port->device, "%s", DEFAULT_SERIAL_DEVICE);	
	printf("port file	:\"%s\"\n", port->device);
	port->buad = DEFAULT_SERIAL_BAUD;
	printf("port buad	:%X\n", port->buad);
	port->timeout = SERIAL_RESPONSE_TIMEOUT;	
	printf("read time	:%d\n", port->timeout);	
	return 1;
}


int init_comm_port(s_comm_port_t *port)
{	
	if(port->fd > 0)
	{//check if serial port is active		
		return 0;
	}	
	if(!port->device)
	{//return if device file is not present
		return 0;
	}			
	if((port->fd = open_serial_port(port->device, port->buad, port->timeout)) > 0)
	{
#if	DEBUG_MODE
		printf("\ndevice @:\"%s\" opened successfully\n\n", port->device);
#endif		
		return port->fd;
	}	
	return 0;
}

int close_comm_port(s_comm_port_t *port)
{	
	if(port->fd < 0)
	{//check if serial port is available to close		
		return 0;
	}	
	close_serial_port(port->fd);
#if DEBUG_MODE
	printf("\ndevice @:\"%s\" closed successfully\n\n", port->device);	
#endif
	return 1;
}

int update_comm_port(s_comm_port_t *port)
{	
	if(!IS_BIT_SET(port->flags, update_port_flag))
	{//check if update required
		return 0;
	}	
	close_comm_port(port);//close comm port before setup again	
	setup_comm_port(port);//setup with new settings	
	init_comm_port(port);//start with new settings	
	return 1;
}

//-------------------write AT commands to the MODEM-----------------------------
int write_to_port(s_comm_port_t *port,  char *data, int len)
{		
	int nbytes = 0;	
	if((nbytes = write_serial_port(port->fd, data, len)) > 0)
	{			
#if DEBUG_MODE
		printf("successfully written to port : %d\n",  nbytes);		
#endif
		return nbytes;
	}	
	reset_serial_port(port->fd);	
#if DEBUG_MODE
	printf("failed to write to port : %d\n", nbytes);	
#endif
	return 0; 
}

int check_read_timer(uptime_timer_t *timer)
{	
	if(!uptime_timer_is_started(timer))
	{	
		return 0;
	}			
	if(!uptime_timer_is_expired(timer))
	{		
		return 0;
	}
	uptime_timer_stop(timer);		
	return 1;
}

//-------------------write AT commands to the MODEM-----------------------------

int read_from_port(s_comm_port_t *port,  char *data, int len)
{				
	int	nbytes = 0;			
	static int rbytes = 0;
	ioctl(port->fd, FIONREAD, &nbytes);	
	if(nbytes <= 0)
	{		
		if(check_read_timer(&inter_byte_timer))
		{		
			nbytes = rbytes;
			rbytes = 0;
			return nbytes;			
		}		
		return 0;			
	}			
	
	nbytes = read_serial_port(port->fd, data + rbytes, 1);	//read one byte at a time	
	uptime_timer_start(&inter_byte_timer, inter_byte_timout);
	rbytes += nbytes;	
	if(rbytes >= len)
	{
		uptime_timer_stop(&inter_byte_timer);	
		nbytes = rbytes;
		rbytes = 0;
		return nbytes;
	}	
	return 0;		/* O - 1 = MODEM ok, -1 = MODEM bad */
}
/*
int read_from_port(s_comm_port_t *port,  packet_struct_type *packet){		
		
	int	nbytes = 1;		

	for(packet->size = 0; nbytes > 0; packet->size += nbytes){			
	
		nbytes = read_serial_port(port->fd, packet->data + packet->size, 1);	//read one byte at a time
		
		if((packet->size + nbytes) > packet->capacity){
			
			break;
		}		
		
		fcntl(port->fd, F_SETFL, 0);//FNDELAY option causes the read function to return 0 if no characters are available on the port. To restore normal (blocking) behavior, call fcntl() without the FNDELAY option:
	}		
	
	fcntl(port->fd, F_SETFL, FNDELAY);
	
	//printf("successfully read from port : %d\n",  vector->size);
		
	return packet->size;		// O - 1 = MODEM ok, -1 = MODEM bad 
}
*/

//-------------------------end of quectel.c-------------------------------------