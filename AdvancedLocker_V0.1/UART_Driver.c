#include <sys/types.h>
#include <sys/stat.h>
#include <sys/select.h>
#include <sys/time.h>
#include <linux/ioctl.h>
#include <termio.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <linux/usbdevice_fs.h>

#include "UART_Driver.h"

#define _POSIX_SOURCE	1				/* POSIX compliant source */

#define FALSE			0
#define TRUE			1

#define CANONICAL		0
#define CONTROL_TTY		0

/* 
	BAUDRATE: Set bps rate. You could also use cfsetispeed and cfsetospeed.
	CRTSCTS : output hardware flow control (only used if the cable has all necessary lines. See sect. 7 of Serial-HOWTO)
	CS8     : 8n1 (8bit,no parity,1 stop bit)
	CLOCAL  : local connection, no modem control
	CREAD   : enable receiving characters
	IGNPAR  : ignore bytes with parity errors
	ICRNL   : map CR to NL (otherwise a CR input on the other computer will not terminate input) otherwise make device raw (no other input processing)
	ICANON  : enable canonical input
*/

int open_serial_port(char *device, int buad, int timeout){
	
	int fd;
	
	struct termios serial;
	
	fd = open(device, O_RDWR | O_NOCTTY | O_NDELAY);	/* Open modem device for reading and writing and not as controlling tty because we don't want to get killed if line noise sends CTRL-C */
	if (fd < 0){
		perror(device);
		return -1; 
	}
	
	fcntl(fd, F_SETFL, FNDELAY);
		
	tcgetattr(fd, &serial);					/* save current serial port settings */
	bzero(&serial, sizeof(serial));			/* clear structure for new port settings */	

#if CANONICAL	
	serial.c_cflag			= buad | CS8 | CLOCAL | CREAD;
	serial.c_iflag			= IGNPAR | ICRNL;	
	serial.c_oflag			= 0;			/* Raw output */
	serial.c_lflag			= ICANON;		/* disable all echo functionality, and don't send signals to calling program */	
	
	serial.c_cc[VTIME]		= 0;			/* inter-character timer unused */
	serial.c_cc[VMIN]		= 1;			/* blocking read until 1 character arrives */
	
#if CONTROL_TTY
	/* initialize all control characters default values can be found in /usr/include/termios.h, and are given in the comments, but we don't need them here */	
	serial.c_cc[VINTR]		= 0;		/* Ctrl-c */ 
	serial.c_cc[VQUIT]		= 0;		/* Ctrl-\ */
	serial.c_cc[VERASE]		= 0;		/* del */
	serial.c_cc[VKILL]		= 0;		/* @ */
	serial.c_cc[VEOF]		= 4;		/* Ctrl-d */
	serial.c_cc[VSWTC]		= 0;		/* '\0' */
	serial.c_cc[VSTART]		= 0;		/* Ctrl-q */ 
	serial.c_cc[VSTOP]		= 0;		/* Ctrl-s */
	serial.c_cc[VSUSP]		= 0;		/* Ctrl-z */
	serial.c_cc[VEOL]		= 0;		/* '\0' */
	serial.c_cc[VREPRINT]	= 0;		/* Ctrl-r */
	serial.c_cc[VDISCARD]	= 0;		/* Ctrl-u */
	serial.c_cc[VWERASE]	= 0;		/* Ctrl-w */
	serial.c_cc[VLNEXT]		= 0;		/* Ctrl-v */
	serial.c_cc[VEOL2]		= 0;		/* '\0' */
#endif		
	
#else
	serial.c_cflag	|= buad | CS8 | CLOCAL | CREAD;
	serial.c_iflag	= IGNPAR;	
	serial.c_oflag	&= ~OPOST;			/* Raw output */
	serial.c_lflag	&= ~(ICANON | ECHO | ECHOE | ISIG);

	serial.c_cc[VTIME]		= timeout;		/* inter-character timer used */
	serial.c_cc[VMIN]		= 0;			/* blocking read until N chars received unused*/
#endif
	
	tcflush(fd, TCIFLUSH);					/* now clean the modem line and activate the settings for the port */
	tcsetattr(fd, TCSANOW, &serial);
	
	return fd;
}

int read_serial_port(int fd, unsigned char *data, int len)
{		
	int ret = 0;			
	if((ret = read(fd, data, len)) < 0)
	{	/* terminal settings done, now handle input	*/	
		//perror("\nread data from serial port failed\n");	
	}	
	return ret;
}

int write_serial_port(int fd, unsigned char *data, int len)
{			
	int ret = 0;		
	if((ret = write(fd, data, len)) < 0)
	{	/* terminal settings done, now send output	*/	
		perror("\nwrite data to serial port failed\n");	
	}	
	tcflush(fd, TCIOFLUSH);	
	return ret;
}

int reset_serial_port(int fd){
	
	int rc = 0;
	//resetting usb device
	rc = ioctl(fd, USBDEVFS_RESET, 0);
	
    if(rc < 0) {
		
        perror("Error in ioctl");
		
        return 0;
    }
	
    printf("Reset successful\n");
	
	return 1;
}

int close_serial_port(int fd){	
	close(fd); 	
	return 1;
}