/* 
 * File:   quectel.h
 * Author: patrick-pc
 *
 * Created on October 8, 2015, 9:28 AM
 */

#ifndef QUECTEL_H
#define	QUECTEL_H

#ifdef	__cplusplus
extern "C"
{
#endif
	
#define SERIAL_DESCRIPTOR_LEN		256
	
#define	SERIAL_RESPONSE_TIMEOUT		25
#define	DEFAULT_SERIAL_BAUD			B38400		/* baudrate settings are defined in <asm/termbits.h>, which is included by <termios.h> */   
#define	DEFAULT_SERIAL_DEVICE		"/dev/ttyUSB0"	/* change this definition for the correct port */
	
#define SET_BIT(flag, bitIndex)	(flag |= (1<<bitIndex))
#define CLR_BIT(flag, bitIndex)	(flag &=~(1<<bitIndex))
#define TOG_BIT(flag, bitIndex)	(flag ^= (1<<bitIndex))
#define IS_BIT_SET(flag, bitIndex)	(flag & (1<<bitIndex))
	
typedef enum COMM_PORT_FLAGS_e 
{
	init_port_flag,		
	setup_port_flag,
	close_port_flag,	
	update_port_flag,
} e_CPOMM_PORT_FLAGS_t;	
	
typedef struct comm_port_s
{	
	int flags;
	int fd;			// file descriptor
	int buad;		// transfer rate
	int timeout;	// end of transfer timeout    
	char device[SERIAL_DESCRIPTOR_LEN];	// device file
} s_comm_port_t;

int setup_comm_port(s_comm_port_t *port);
	
int init_comm_port(s_comm_port_t *port);

int close_comm_port(s_comm_port_t *port);

int update_comm_port(s_comm_port_t *port);

//-------------------write AT commands to the MODEM-----------------------------
int write_to_port(s_comm_port_t *port,  char *data, int len);

//-------------------write AT commands to the MODEM-----------------------------
int read_from_port(s_comm_port_t *port,  char *data, int len);

#ifdef	__cplusplus
}
#endif

#endif	/* QUECTEL_H */

