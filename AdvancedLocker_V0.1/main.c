/* A simple server in the internet domain using TCP.
myServer.c
D. Thiebaut
Adapted from http://www.cs.rpi.edu/~moorthy/Courses/os98/Pgms/socket.html
The port number used in 51717.
This code is compiled and run on the Raspberry as follows:
   
    g++ -o myServer myServer.c 
    ./myServer

The server waits for a connection request from a client.
The server assumes the client will send positive integers, which it sends back multiplied by 2.
If the server receives -1 it closes the socket with the client.
If the server receives -2, it exits.
*/

#include <stdio.h>
#include <sys/types.h> 
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <bcm2835.h>
#include "UART_Handler.h"

#define BUFFER_LEN		256

void error( char *msg ) {
  perror(  msg );
  exit(1);
}

int func( int a ) {
   return 2 * a;
}

void sendData( int sockfd, char *data , int len ) {
  int n;
  if ( (n = write( sockfd, data, len ) ) < 0 )
  {
	 error(  "ERROR reading from socket" );
  }  
}

int getData( int sockfd , char *data , int len) { 
  int n;
  if ( (n = read(sockfd, data, len) ) < 0 )
  {
    error( "ERROR reading from socket" );
  }
  data[n] = '\0';
  return n;
}

int main(int argc, char *argv[]) {
     int sockfd, newsockfd, portno = 2031, clilen;
     char buffer[BUFFER_LEN];
     struct sockaddr_in serv_addr, cli_addr;
     int n;   
	 
	 s_comm_port_t UART_Comm;
	 
	 setup_comm_port( &UART_Comm );		
	 init_comm_port( &UART_Comm );//start with new settings	

     printf( "using port #%d\n", portno );
    
     sockfd = socket( AF_INET, SOCK_STREAM, 0 );
     if (sockfd < 0) 
     {
		error( "ERROR reading from socket" );
	 }
     bzero( (char *) &serv_addr, sizeof(serv_addr) );

     serv_addr.sin_family = AF_INET;
     serv_addr.sin_addr.s_addr = INADDR_ANY;
     serv_addr.sin_port = htons( portno );
     if (bind( sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr) ) < 0) 
	 {
		error( "ERROR reading from socket" );
	 }
     listen( sockfd, 5 );
     clilen = sizeof(cli_addr);
  
     //--- infinite wait on a connection ---
     while ( 1 ) {
        printf( "waiting for new client...\n" );
        if ( ( newsockfd = accept( sockfd, (struct sockaddr *) &cli_addr, (socklen_t*) &clilen) ) < 0 )
		{
			error( "ERROR reading from socket" );
			close( newsockfd );
			break; 
		}
        printf( "opened new communication with client\n" );
        while ( 1 ) {
	    //---- wait for a number from client ---
            n = getData( newsockfd , buffer, BUFFER_LEN);
            printf( "got %s\n", buffer );
            if ( buffer[0] == 'X' ) 
				break;                        
			//--- send new data back --- 
			printf( "sending back %s\n", buffer );
            //sendData( newsockfd, buffer, n );			
			//--- send new data to UART --- 
			//write_to_port( &UART_Comm, buffer, n );
		}
        close( newsockfd );
		break;       
     }
     return 0; 
}